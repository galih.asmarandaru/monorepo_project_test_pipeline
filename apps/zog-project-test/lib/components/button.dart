import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_color/state_color.dart';
import 'package:flutter/material.dart';

class ButtonRounded extends StatelessWidget {
  final Function onPressed;
  final Widget text;
  final double width;

  const ButtonRounded({Key key, this.onPressed, this.text, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocBuilder<ColorMode, Color>(
      builder: (context, currentColor) => Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        width: size.width * width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: currentColor,
            boxShadow: [
              BoxShadow(
                color: Colors.grey[500],
                offset: Offset(0.0, 1.5),
                blurRadius: 1.5,
              ),
            ]),
        child: TextButton(
            key: Key("compButtonRounded"),
            style: TextButton.styleFrom(
              padding: EdgeInsets.all(0.0),
              primary: Colors.white,
            ),
            onPressed: onPressed,
            child: text),
      ),
    );
  }
}

class ButtonBack extends StatelessWidget {
  final String text;
  final Function onPressed;
  const ButtonBack({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Transform.scale(
            scale: 2,
            child: IconButton(
                key: Key("compButtonBack"),
                icon: ImageIcon(
                  AssetImage("assets/img/ic-left-arrow.png"),
                  color: Colors.white,
                  size: 10,
                ),
                onPressed: onPressed),
          ),
          Text(
            text,
            style: TextStyle(
                color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
