import 'package:test/test.dart';
import 'package:zog_project_test/utils/counter.dart';

void main() {
  test('Counter value should be incremented', () {
    final counter = Counter();

    counter.increment();

    expect(counter.value, 1);
  });
}
