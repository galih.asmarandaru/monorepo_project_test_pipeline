// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:state_color/state_color.dart';
import 'package:zog_project_test/components/button.dart';

import 'package:zog_project_test/main.dart';

void main() {
  testWidgets('Get my Page', (WidgetTester tester) async {
    // Variable
    var dString = "Add String";

    // Get Widgets
    final compButtonRounded = find.byKey(ValueKey("compButtonRounded"));
    final compButtonBack = find.byKey(ValueKey("compButtonBack"));
    final compTextField = find.byKey(ValueKey("compTextField"));
    final compCheckbox = find.byKey(ValueKey("compCheckbox"));
    final compSwitch = find.byKey(ValueKey("compSwitch"));

    // Execute Test
    await tester.pumpWidget(MyApp());

    await tester.tap(compButtonRounded);
    await tester.tap(compButtonBack);

    await tester.enterText(compTextField, dString);

    await tester.tap(compCheckbox);

    await tester.tap(compSwitch);

    await tester.pump();

    // Checking Output
    expect(compButtonRounded, findsOneWidget);
    expect(compButtonBack, findsOneWidget);

    expect(find.text(dString), findsOneWidget);

    expect(compCheckbox, findsOneWidget);

    expect(compSwitch, findsOneWidget);
  });
}
